/**
 * View Models used by Spring MVC REST controllers.
 */
package com.idsmart.checkevent.web.rest.vm;
